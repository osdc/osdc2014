<?php
/**
 * @file
 * osdc_presenter.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function osdc_presenter_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function osdc_presenter_node_info() {
  $items = array(
    'presentation' => array(
      'name' => t('Presentation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Talk Title'),
      'help' => t('Thanks for submitting a presentation proposal to the Open Source Developers\' Conference. Please complete the following form, and you\'ll be added to the queue for deliberation. We suggest you read <a href="http://osdc.local/getting-your-conference-abstract-proposal-accepted">Jacinta Richardson\'s article <em>Getting your Conference Abstract / Proposal Accepted</em></a> for hints and tips, in order to increase your chances of being accepted.'),
    ),
  );
  return $items;
}
