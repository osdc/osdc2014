<?php
/**
 * @file
 * osdc_presenter.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function osdc_presenter_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create presentation content'.
  $permissions['create presentation content'] = array(
    'name' => 'create presentation content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
