<?php
/**
 * @file
 * osdc_presenter.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function osdc_presenter_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['afb-1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 1,
    'module' => 'afb',
    'node_types' => array(),
    'pages' => 'cfp',
    'roles' => array(),
    'themes' => array(
      'osdc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'osdc',
        'weight' => -10,
      ),
    ),
    'title' => 'Submit Your Proposal',
    'visibility' => 1,
  );

  return $export;
}
