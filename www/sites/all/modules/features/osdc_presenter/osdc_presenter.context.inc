<?php
/**
 * @file
 * osdc_presenter.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function osdc_presenter_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'cfp';
  $context->description = '';
  $context->tag = 'CFP';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'cfp' => 'cfp',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'afb-1' => array(
          'module' => 'afb',
          'delta' => '1',
          'region' => 'content_aside',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('CFP');
  $export['cfp'] = $context;

  return $export;
}
