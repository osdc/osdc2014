<?php
/**
 * @file
 * osdc_presenter.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function osdc_presenter_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_call-for-proposals:node/13
  $menu_links['main-menu_call-for-proposals:node/13'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/13',
    'router_path' => 'node/%',
    'link_title' => 'Call for Proposals',
    'options' => array(
      'identifier' => 'main-menu_call-for-proposals:node/13',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Call for Proposals');


  return $menu_links;
}
