<?php
/**
 * @file
 * osdc_presenter.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function osdc_presenter_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-presentation-body'
  $field_instances['node-presentation-body'] = array(
    'bundle' => 'presentation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Tell us more about your talk. Read <a href="/getting-your-conference-abstract-proposal-accepted">Jacinta Richardson\'s article <em>Getting your Conference Abstract / Proposal Accepted</em></a> for hints and tips.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Synopsis',
    'required' => 1,
    'settings' => array(
      'display_summary' => 0,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => -4,
    ),
  );

  // Exported field_instance: 'node-presentation-field_presentation_format'
  $field_instances['node-presentation-field_presentation_format'] = array(
    'bundle' => 'presentation',
    'default_value' => array(
      0 => array(
        'tid' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_presentation_format',
    'label' => 'Presentation Format',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => -2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Presentation Format');
  t('Synopsis');
  t('Tell us more about your talk. Read <a href="/getting-your-conference-abstract-proposal-accepted">Jacinta Richardson\'s article <em>Getting your Conference Abstract / Proposal Accepted</em></a> for hints and tips.');

  return $field_instances;
}
