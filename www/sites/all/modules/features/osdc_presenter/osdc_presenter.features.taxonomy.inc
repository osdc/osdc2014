<?php
/**
 * @file
 * osdc_presenter.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function osdc_presenter_taxonomy_default_vocabularies() {
  return array(
    'presentation_format' => array(
      'name' => 'Presentation Format',
      'machine_name' => 'presentation_format',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
