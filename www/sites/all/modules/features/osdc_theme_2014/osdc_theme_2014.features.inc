<?php
/**
 * @file
 * osdc_theme_2014.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function osdc_theme_2014_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
