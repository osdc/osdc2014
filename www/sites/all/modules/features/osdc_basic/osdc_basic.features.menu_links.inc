<?php
/**
 * @file
 * osdc_basic.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function osdc_basic_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_home:<front>
  $menu_links['navigation_home:<front>'] = array(
    'menu_name' => 'navigation',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Home');


  return $menu_links;
}
