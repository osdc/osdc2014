<?php
/**
 * @file
 * osdc_setup.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function osdc_setup_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_entityform_type().
 */
function osdc_setup_default_entityform_type() {
  $items = array();
  $items['contact'] = entity_import('entityform_type', '{
    "type" : "contact",
    "label" : "Contact",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "html" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "html" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2" },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : {
        "value" : "\\u003Cp style=\\u0022margin-top: 0px; margin-bottom: 1.5em; font-family: \\u0027Trebuchet MS\\u0027, \\u0027Helvetica Neue\\u0027, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 21px;\\u0022\\u003EContact form\\u003C\\/p\\u003E\\r\\n",
        "format" : "html"
      }
    },
    "weight" : "0",
    "paths" : {
      "submit" : {
        "source" : "eform\\/submit\\/contact",
        "alias" : "contact",
        "language" : "und"
      },
      "confirm" : {
        "source" : "eform\\/contact\\/confirm",
        "alias" : "contact\\/thanks",
        "language" : "und"
      }
    }
  }');
  return $items;
}
