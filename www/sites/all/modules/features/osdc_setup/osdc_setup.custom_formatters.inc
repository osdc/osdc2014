<?php
/**
 * @file
 * osdc_setup.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function osdc_setup_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'user_page_heading';
  $formatter->label = 'User Page Heading';
  $formatter->description = '';
  $formatter->mode = 'token';
  $formatter->field_types = 'text';
  $formatter->code = '<h1>[user:field_fullname]</h1>';
  $formatter->fapi = '';
  $export['user_page_heading'] = $formatter;

  return $export;
}
