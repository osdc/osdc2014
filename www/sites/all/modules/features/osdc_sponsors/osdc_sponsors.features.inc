<?php
/**
 * @file
 * osdc_sponsors.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function osdc_sponsors_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function osdc_sponsors_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_entityform_type().
 */
function osdc_sponsors_default_entityform_type() {
  $items = array();
  $items['sponsorship_inquiry'] = entity_import('entityform_type', '{
    "type" : "sponsorship_inquiry",
    "label" : "Sponsors",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "html" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "html" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2" },
      "resubmit_action" : "old",
      "redirect_path" : "",
      "instruction_pre" : {
        "value" : "\\u003Cp style=\\u0022margin-top: 0px; margin-bottom: 1.5em; font-family: \\u0027Trebuchet MS\\u0027, \\u0027Helvetica Neue\\u0027, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 21px;\\u0022\\u003ESupporting OSDC is a fantastic opportunity to provide exposure of your business or service to the open source industry, and also to support that industry by providing access to an essential conference on the Australian technical landscape.\\u003C\\/p\\u003E\\r\\n\\u003Cp style=\\u0022margin-top: 0px; margin-bottom: 1.5em; font-family: \\u0027Trebuchet MS\\u0027, \\u0027Helvetica Neue\\u0027, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 21px;\\u0022\\u003EWe\\u0027re looking for\\u003Cspan style=\\u0022line-height: 1.5;\\u0022\\u003E\\u0026nbsp;organisations that we can work with to create a bilateral strategies that will benefit all involved, and\\u003C\\/span\\u003E\\u003Cspan style=\\u0022line-height: 1.5;\\u0022\\u003E\\u0026nbsp;maximising the returns on investment. Some examples of ways previous sponsors have benefitted from sponsoring OSDC include:\\u003C\\/span\\u003E\\u003C\\/p\\u003E\\r\\n\\u003Cul style=\\u0022margin-top: 1em; margin-bottom: 1em; margin-left: 0px; padding-left: 40px; font-family: \\u0027Trebuchet MS\\u0027, \\u0027Helvetica Neue\\u0027, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 21px;\\u0022\\u003E\\r\\n\\t\\u003Cli\\u003Egaining exposure to a technical crowd through media campaigns and promotions;\\u003C\\/li\\u003E\\r\\n\\t\\u003Cli\\u003Einnovative sponsor-supplied items in conference bags providing a talking-point and strong direct marketing opportunities to a target audience;\\u003C\\/li\\u003E\\r\\n\\t\\u003Cli\\u003Eusing their on-site presence as an opportunity to attract and interview prospective staff during the event.\\u003C\\/li\\u003E\\r\\n\\u003C\\/ul\\u003E\\r\\n\\u003Cp style=\\u0022margin-top: 0px; margin-bottom: 1.5em; font-family: \\u0027Trebuchet MS\\u0027, \\u0027Helvetica Neue\\u0027, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 21px;\\u0022\\u003EWe have several levels of investment which are detailed in the Sponsor Pack, and encourage you to get in touch to work out out we can customise the packages to best suit your requirements.\\u003C\\/p\\u003E\\r\\n",
        "format" : "html"
      }
    },
    "weight" : "0",
    "paths" : {
      "submit" : {
        "source" : "eform\\/submit\\/sponsorship-inquiry",
        "alias" : "sponsors",
        "language" : "und"
      },
      "confirm" : {
        "source" : "eform\\/sponsorship-inquiry\\/confirm",
        "alias" : "sponsors\\/thanks",
        "language" : "und"
      }
    }
  }');
  return $items;
}

/**
 * Implements hook_node_info().
 */
function osdc_sponsors_node_info() {
  $items = array(
    'sponsor' => array(
      'name' => t('Sponsor'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
