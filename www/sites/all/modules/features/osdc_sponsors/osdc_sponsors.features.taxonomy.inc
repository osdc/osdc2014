<?php
/**
 * @file
 * osdc_sponsors.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function osdc_sponsors_taxonomy_default_vocabularies() {
  return array(
    'sponsor_level' => array(
      'name' => 'Sponsor Level',
      'machine_name' => 'sponsor_level',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
