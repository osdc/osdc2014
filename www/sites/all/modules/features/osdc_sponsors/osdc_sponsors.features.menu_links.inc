<?php
/**
 * @file
 * osdc_sponsors.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function osdc_sponsors_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_sponsors:sponsors
  $menu_links['navigation_sponsors:sponsors'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'sponsors',
    'router_path' => 'sponsors',
    'link_title' => 'Sponsors',
    'options' => array(
      'identifier' => 'navigation_sponsors:sponsors',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Sponsors');


  return $menu_links;
}
